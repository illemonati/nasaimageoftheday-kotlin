package nasaimageoftheday

import javafx.application.Application
import javafx.scene.Parent
import javafx.stage.Stage
import javafx.fxml.FXMLLoader
import javafx.scene.Scene

class GuiMain : Application() {
    override fun start(primaryStage: Stage) {
        val rootLoader= FXMLLoader(this::class.java.getResource("mainFXML.fxml"))
        val root = rootLoader.load<Parent>()
        val mainScene = Scene(root)
        primaryStage.scene = mainScene
        primaryStage.title = "Nasa Image of The Day"
        primaryStage.show()
    }

}

fun main(args: Array<String>) {
    Application.launch(GuiMain::class.java, *args)
}
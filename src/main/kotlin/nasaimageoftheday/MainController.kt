package nasaimageoftheday


import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.Region
import javafx.scene.layout.VBox
import kotlinx.coroutines.*
import kotlinx.coroutines.javafx.JavaFx
import kotlin.coroutines.CoroutineContext

class MainController() : CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.JavaFx

    @FXML
    private lateinit var mainVBox: VBox

    @FXML
    private lateinit var title : Label

    @FXML
    private lateinit var description : Label

    @FXML
    private lateinit var image: ImageView

    @FXML
    private lateinit var time: Label

    @FXML
    private lateinit var link: Label

    @FXML
    private fun initialize() {
        title.text = "Loading"
        description.text = ""
        time.text = ""
        link.text = ""
        description.minHeight(description.prefHeight)
        launch {
            val imotd = getIMOTD()
            title.text = imotd.title
            description.text = imotd.description
            image.image = Image(imotd.imageUrl.replaceRange(0..3, "https"))
            time.text = imotd.time
            link.text = imotd.link
        }
    }

}
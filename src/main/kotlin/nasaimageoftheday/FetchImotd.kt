package nasaimageoftheday

import com.rometools.rome.io.SyndFeedInput
import com.rometools.rome.io.XmlReader
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.request.get
import io.ktor.utils.io.ByteReadChannel
import java.io.InputStream

suspend fun fetchIMOTD(): InputStream {
    val imotdLink = "https://www.nasa.gov/rss/dyn/lg_image_of_the_day.rss"
    val client = HttpClient(Apache)
    return client.get(imotdLink)
}

data class IMOTD(val title: String, val description: String, val imageUrl: String, val time: String, val link: String)

suspend fun getIMOTD() : IMOTD {
    val rawFeed = fetchIMOTD()
    val feed = SyndFeedInput().build(XmlReader(rawFeed))
    val current = feed.entries[0]
    return IMOTD(
        current.title,
        current.description.value,
        current.enclosures[0].url,
        current.publishedDate.toString(),
        current.uri
    )
}